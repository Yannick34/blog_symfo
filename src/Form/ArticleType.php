<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class)
            ->add('content', TextareaType::class, ['label' => 'Contenu'])
            //->add('image', TextType::class)
            ->add('image', FileType::class,[
                'mapped' => false, 
                'required' => false, 
                'label' => 'Avatar (formats : png, jpg ou jpeg / Max : 2Mo)',
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/jpg',
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Veuillez entrer une image de 2 Mo maximum et au format jpg, jpeg ou png',
                    ])
                ],
                ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'title', 
                'label' => 'Catégorie'
            ])
            //->add('createdAt')
            //['choices' => ['Catégorie 1' => 'premier', 'Catégorie 2' => 'deuxieme','Catégorie 3' => 'deuxieme']

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
