<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class AvatarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('email')
            // ->add('isActive')
            // ->add('roles')
            ->add('avatar', FileType::class,[
                'mapped' => false, 
                'required' => false, 
                'label' => 'Avatar (formats : png, jpg ou jpeg / Max : 2Mo)',
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/jpg',
                            'image/jpeg',
                            'image/png'
                        ],
                        'mimeTypesMessage' => 'Veuillez entrer une image de 2 Mo maximum et au format jpg, jpeg ou png',
                    ])
                ],
                ])
            ->add('submit', SubmitType::class, ['label'=>'Valider', 'attr'=>['class'=>'btn-primary btn-block']])
    ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

// 'required' => false, 
//                 'label' => 'Avatar (image au format png, jpg ou jpeg)',
//                 'constraints' => [
//                     new File([
//                         'maxSize' => '2048k',
//                         'mimeTypes' => [
//                             'image/jpg',
//                             'image/jpeg',
//                             'image/png'
//                         ],
//                         'mimeTypesMessage' => 'Veuillez entrer une image de 2 Mo maximum et au format jpg, jpeg ou png',
//                     ])
//                 ],