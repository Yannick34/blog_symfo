<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Category;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Repository\CategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ArticleType;
use App\Form\CommentType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class CommentController extends AbstractController
{

     /**
     * @Route("/post_comment/{id}", name="post_comment")
     */
    public function createComment(Article $article = null, Comment $comment = null, Request $request, ObjectManager $manager,ArticleRepository $repo, CommentRepository $repo2, $id)
    {
        //Article $article = null
        //$view_article = $this->getDoctrine()->getRepository(Article::class);
        $view_article=$repo->find($id);
        //$view_article->getComments();
        $view_comments=$repo2->findByArticle($id);
        
        if($this->getUser()->getPseudo() !== null){
            $user = $this->getUser()->getPseudo();
        } else {
            $user = $this->getUser()->getEmail();
        }
       

        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

           if($form->isSubmitted() && $form->isValid()){
                   $comment->setCreatedAt(new \DateTime);
                   $comment->setArticle($view_article);
                   $comment->setAuthor($user);
                   

           $manager->persist($comment);
           $manager->flush();
           $this->addFlash('success', 'Votre commentaire a bien été enregistré.');

           return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
           }

        return $this->render('blog/comments/comment.html.twig',[
            'formComment'=> $form->createView(),
            'view_article' => $view_article,
            'view_comments' => $view_comments
            ]);
    }

    /**
     * @Route("{id}/{idComment}/comment/delete", name="delete_comment")
     */
    public function delete_comment($id, $idComment, Article $article, EntityManagerInterface $em, CommentRepository $repo)
    {
        $comment = $repo->find($idComment);
        $em->remove($comment);
        $em->flush();
        return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        //return $this->render('blog/delete_article.html.twig', []);
    }
}

