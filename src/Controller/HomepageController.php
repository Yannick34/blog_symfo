<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;

use App\Entity\Article;

class HomepageController extends Controller {

    /**
     * @Route("/")
     */
    public function index(ArticleRepository $repo) {

        $articles = $repo->findBy(array(), array('createdAt' => 'desc'));
        
       

        return $this->render('blog/index.html.twig', ['mainNavHome'=>true, 'title'=>'Accueil', 'articles' => $articles]);
    }

}