<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Article;
use App\Repository\CategoryRepository;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;


class CategoryController extends AbstractController
{

    /**
     * @Route("blog/categories/{idCat}", name="categories_art")
     */
    public function showArt_Categories(CategoryRepository $repo3, ArticleRepository $repo, $idCat)
    {
        //$view_article = $this->getDoctrine()->getRepository(Article::class);
        $viewArt_categories=$repo3->find($idCat);
        //array('createdAt' => 'desc')
        dump($viewArt_categories);
      

        return $this->render('blog/categories/cat_Art.html.twig', [
            'viewArt_categories' => $viewArt_categories,
        ]);
    }

    /**
     * @Route("blog/categories", name="categories")
     */
    public function show_categories(CategoryRepository $repo3)
    {

        //$view_article = $this->getDoctrine()->getRepository(Article::class);
        $view_categories=$repo3->findAll();

        return $this->render('blog/categories/categories.html.twig', [
            'view_categories' => $view_categories
        ]);
    }

}