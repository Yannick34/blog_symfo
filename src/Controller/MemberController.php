<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Entity\Article;
use App\Entity\Comment;

use App\Repository\UserRepository;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;

use App\Controller\UserInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use App\Controller\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Form\AvatarType;

use App\Service\FileUploader;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;




/** @Route("/member") */
class MemberController extends Controller {

    /**
     * @Route("/")
     */
    public function index() {
        $user = $this->getUser();

        return $this->render('member/index.html.twig', ['mainNavMember'=>true, 'title'=>'Espace Membre', 'user' => $user]);
    }

    /**
     * @Route("/mes_articles", name="member_articles")
     */
    public function member_articles(ArticleRepository $repo) {
        $user = $this->getUser();
        $pseudo = $user->getPseudo();
        $articles = $repo->findByAuthor($pseudo);

        return $this->render('member/mes_articles/mes_articles.html.twig', ['mainNavMember'=>true, 'title'=>'Espace Membre', 'pseudo' => $pseudo, 'articles' => $articles]);
    }


    /**
     * @Route("/mes_commentaires", name="member_commentaires")
     */
    public function member_commentaires(CommentRepository $repo) {
        $user = $this->getUser();
        $pseudo = $user->getPseudo();
        $comments = $repo->findByAuthor($pseudo);

        return $this->render('member/mes_commentaires/mes_commentaires.html.twig', ['mainNavMember'=>true, 'title'=>'Espace Membre', 'pseudo' => $pseudo, 'comments' => $comments]);
    }

    /**
     * @Route("/personnalise", name="personnalise")
     */
    public function personnalise(Request $request, FileUploader $fileUploader,EntityManagerInterface $em)
    {
      
        $userTotal = $this->getUser();
        $form = $this->createForm(AvatarType::class, $userTotal);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $avatarFile */
    
            $em = $this->getDoctrine()->getManager();
            $avatarFile = $form['avatar']->getData();
            if ($avatarFile) {
                $avatarFileName = $fileUploader->upload($avatarFile);
                $userTotal->setAvatar($avatarFileName);
            }
    
            $em->persist($userTotal);
            $em->flush();
            $this->addFlash('success', 'Votre avatar a bien été modifié.');
                
                // if($file){
                //     $filename = md5(uniqid()).'.'.$file->guessExtension();
                //     $file->move(
                //         $this->getParameter('avatars_directory'),
                //         $filename
                //     );
                // }
    
    
    
            // $avatarFile = $form->getData();
            // if ($avatarFile) {
            //     $originalFilename = pathinfo($avatarFile->getClientOriginalName(), PATHINFO_FILENAME);
            //     // this is needed to safely include the file name as part of the URL
            //     $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            //     $newFilename = $safeFilename.'-'.uniqid().'.'.$avatarFile->guessExtension();
    
                return $this->redirect($this->generateUrl('personnalise'));
            }
            return $this->render('member/personnalisation/personnalisation.html.twig', ['mainNavMember'=>true, 'title'=>'Espace Membre', 'userTotal' => $userTotal, 'form' => $form->createView()]);
            }
        }




    // public function personnalise(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder) {

    //     $userTotal = $this->getUser();
    //     //$user = new User();
    //     $form = $this->createForm(AvatarType::class, $userTotal);
    //     $form->handleRequest($request);
    //     //&& $form->isValid()

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $em = $this->getDoctrine()->getManager();
    //         //$file = $request->files->get('user')['avatar'];
    //         dump($request);
    //         $file = $form->getData();
            
    //         if($file){
    //             $filename = md5(uniqid()).'.'.$file->guessExtension();
    //             $file->move(
    //                 $this->getParameter('avatars_directory'),
    //                 $filename
    //             );
    //         }
    //         $userTotal->setAvatar($filename);
    //         $em->persist($userTotal);
    //         $em->flush();

    


        //gestion d'avatars
    // if(isset($_FILES['avatar']) AND !empty($_FILES['avatar']['name'])) {
    //     $tailleMax = 2097152; //2Mo
    //     $extensionsValides = array('jpg', 'jpeg', 'gif', 'png');
    //     if($_FILES['avatar']['size'] <= $tailleMax) {
    //        $extensionUpload = strtolower(substr(strrchr($_FILES['avatar']['name'], '.'), 1));
    //        if(in_array($extensionUpload, $extensionsValides)) {
    //           $chemin = "membres/avatars/".$_SESSION['id'].".".$extensionUpload;
    //           $resultat = move_uploaded_file($_FILES['avatar']['tmp_name'], $chemin);
    //           if($resultat) {
    //              $updateAvatar = $bdd->prepare('UPDATE membres SET avatar = :avatar WHERE id = :id');
    //              $updateAvatar->execute(array(
    //                 'avatar' => $_SESSION['id'].".".$extensionUpload,
    //                 'id' => $_SESSION['id']
    //                 ));
    //              header('Location: profil.php?id='.$_SESSION['id']);
    //           } else {
    //              $msg = "Erreur durant l'importation de votre photo de profil";
    //           }
    //        } else {
    //           $msg = "Votre photo de profil doit être au format jpg, jpeg, gif ou png";
    //        }
    //     } else {
    //        $msg = "Votre photo de profil ne doit pas dépasser 2Mo";
    //     }
    //  }
