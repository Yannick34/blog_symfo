<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\Category;

use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Repository\CategoryRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ArticleType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Service\FileUploader;


class ArticleController extends AbstractController
{

     /**
     * @Route("/watch_article/{id}", name="blog_show")
     */
    public function show(ArticleRepository $repo, CommentRepository $repo2, $id)
    {
        $view_article=$repo->find($id);
        //$view_article->getComments();
        $view_comments=$repo2->findByArticle($id, array('createdAt' => 'desc'));
        return $this->render("blog/articles/articles_show.html.twig", [
            'view_article' => $view_article,
            'view_comments' => $view_comments
            
        ]);
    }

    /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function create(Article $article = null, Request $request, ObjectManager $manager, CategoryRepository $repo3, EntityManagerInterface $em, FileUploader $fileUploader)
    {

        $view_categories=$repo3->findAll();
        $user = $this->getUser();
        $pseudo = $user->getPseudo();
    

        if(!$article){
            $article = new Article();
        }

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

           if($form->isSubmitted() && $form->isValid()){
               /** @var UploadedFile $imageFile */
               
               if(!$article->getId()){
                   $article->setCreatedAt(new \DateTime);
                   $article->setAuthor($pseudo);
                   $article->setUser($this->getUser());
                   $em = $this->getDoctrine()->getManager();
                   $articleFile = $form['image']->getData();
                   if ($articleFile) {
                       $articleFileName = $fileUploader->upload($articleFile);
                       $article->setImage($articleFileName);
                   }
                }

           $manager->persist($article);
           $manager->flush();

           $this->addFlash('success', 'Votre article a bien été enregistré.');
           return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
           }

        return $this->render('blog/articles/create.html.twig',[
            'formArticle'=> $form->createView(),
            'editMode' => $article->getId() !== null, //methode special
            'view_categories' => $view_categories,
            'user' => $user
            ]);

    }

    /**
     * @Route("/blog/{id}/delete", name="delete_article")
     */
    public function delete_article(ObjectManager $manager, Article $article = null)
    {
        $manager->remove($article);
        $manager->flush();
        return $this->redirectToRoute('blog');
        //return $this->render('blog/delete_article.html.twig', []);
    }

    /**
     * @Route("/blog", name="blog")
     */
    public function index(ArticleRepository $repo)
    {
        $articles = $repo->findAll();
        return $this->render('blog/index.html.twig', [
            'articles' => $articles
        ]);
    }
}


//$view_article = $this->getDoctrine()->getRepository(Article::class);
//$repo = $this->getDoctrine()->getRepository(Article::class);//cette ligne ou le parametre + le use

        //$article = $repo.find(8); pour chercher un seul article par article
        //$article = $repo.findOneByTitre("Titre de l'article n° 1");
        //$article = $repo.findByTitle('titre');


 // dump($request);

        // if($request->request->count() > 2 && $request->request->get('titre') !== "" ){
        //     $article = new Article();
        //     $article->setTitre($request->request->get('titre'))
        //             ->setContent($request->request->get('content'))
        //             ->setImage($request->request->get('image'))
        //             ->setCreatedAt(new \DateTime());

        //     $manager->persist($article);
        //     $manager->flush();

        //     return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);

        // }
        // return $this->render('blog/create.html.twig', [
        // ]);
        // $form = $this->createFormBuilder($article)
        //              ->add('titre', TextType::class, ['attr' => ['placeholder' => "Titre de l'article"]])
        //              ->add('content', TextareaType::class, ['attr' => ['placeholder' => "Contenu de l'article"]])
        //              ->add('image', TextType::class, ['attr' => ['placeholder' => "Lien de l'image"]])
        //              ->getForm();

        // $form = $this->createFormBuilder($article)
        //              ->add('titre', TextType::class)
        //              ->add('content', TextareaType::class)
        //              ->add('image', TextType::class)
        //              //->add('save', SubmitType::class, ['label' => 'Créer'])
        //              ->getForm();

        //              $form->handleRequest($request);
