<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Entity\Comment;
use App\Entity\Article;

use App\Repository\UserRepository;
use App\Repository\CommentRepository;
use App\Repository\ArticleRepository;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

/** @Route("/admin") */
class HomepageController extends Controller {

    /**
     * @Route("/")
     */
    public function index() {
        $user = $this->getUser();
        return $this->render('admin/homepage/index.html.twig', ['mainNavAdmin' => true, 'title' => 'Espace Admin', 'user' => $user]);
    }

    /**
     * @Route("/comptes", name="admin_comptes")
     */
    public function comptes(UserRepository $repo) {
        
        $view_members=$repo->findBy(array(), array('id' => 'asc'));;
        return $this->render('admin/vue_comptes/vue_comptes.html.twig', ['mainNavAdmin' => true, 'title' => 'Espace Admin', 'view_members' => $view_members]);
    }

    /**
     * @Route("/comptes/delete/{id}", name="delete_compte")
     */
    public function delete_compte(UserRepository $repo, ObjectManager $manager, User $user) {
        $manager->remove($user);
        $manager->flush();
        $this->addFlash('success', 'Le compte a bien été supprimé');
        return $this->redirectToRoute('admin_comptes');
    }

    /**
     * @Route("/commentaires", name="admin_commentaires")
     */
    public function commentaires(CommentRepository $repo) {
        
        $view_commentaires=$repo->findBy(array(), array('createdAt' => 'desc'));
        return $this->render('admin/vue_commentaires/vue_commentaires.html.twig', ['mainNavAdmin' => true, 'title' => 'Espace Admin', 'view_commentaires' => $view_commentaires]);
    }

    /**
     * @Route("/commentaires/delete/{id}", name="delete_commentaire")
     */
    public function delete_commentaire(CommentRepository $repo, ObjectManager $manager, Comment $comment) {
        
        $manager->remove($comment);
        $manager->flush();
        $this->addFlash('success', 'Le commentaire a bien été supprimé');
        return $this->redirectToRoute('admin_commentaires');
    }

    /**
     * @Route("/articles", name="admin_articles")
     */
    public function articles(ArticleRepository $repo) {
        
        $view_articles=$repo->findBy(array(), array('createdAt' => 'desc'));
        return $this->render('admin/vue_articles/vue_articles.html.twig', ['mainNavAdmin' => true, 'title' => 'Espace Admin', 'view_articles' => $view_articles]);
    }

    /**
     * @Route("/articles/delete/{id}", name="delete_article")
     */
    public function delete_article(CommentRepository $repo, ObjectManager $manager, Article $article) {
        
        $manager->remove($article);
        $manager->flush();
        $this->addFlash('success', 'L\'article a bien été supprimé');
        return $this->redirectToRoute('admin_articles');
    }

}