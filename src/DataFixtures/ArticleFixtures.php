<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use Faker;


class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Faker\Factory::create('fr_FR');

        for($i=0; $i<5; $i++){

            $category = new Category();
            $category->setTitle($faker->sentence())
                     ->setDescription($faker->paragraph());

            $manager->persist($category);

            for($j=0; $j<6; $j++){
                $article = new Article();
                $article->setTitre($faker->sentence())
                        ->setContent($faker->text())
                        ->setImage($faker->imageUrl($width = 350, $height = 250))
                        ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                        ->setCategory($category);

                //astuce pour mettre la date du com après l'article
                $now= new \DateTime();
                $interval = $now->diff($article->getCreatedAt());
                $days = $interval->days;
                $minimum = '-'.$days.' days';

                $manager->persist($article);

                for($k=0; $k<5; $k++){
                    $comment = new Comment();

                    $comment->setAuthor($faker->sentence())
                            ->setContent($faker->text($maxNbChars = 100))
                            ->setCreatedAt($faker->dateTimeBetween($minimum))
                            ->setArticle($article);

                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }
}
